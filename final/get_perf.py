import matplotlib.pyplot as plt
import numpy as np
import os
import sys
import csv

def get_gpu_usage(input):
	with open(input,'r') as f:
		content = csv.reader(f, delimiter=',') 
		# dtype={'names': ('device', 'type', 'timestamp', 'usage'),
		# 	   'formats':('i4','U25','i4','i4')})
		x = list(content)
		x = x[2:]
		newx = []
		
		for i,row in enumerate(x):
			if row[0] == "GPU activities":
				newx.append(row)
		print(newx)
	# 	x = np.array(x).astype('float')
	# m, n = x.shape
	# return m, x[:,-1]

if __name__ == '__main__':
	res = sys.argv[1]
	# typ = sys.argv[2]
	get_gpu_usage(res)
	# m, utils = get_gpu_usage(res)
	# time = np.arange(m)
	# plt.plot(time, utils, linewidth=1.5)
	# plt.xlabel('time', fontsize=16)
	# plt.ylabel('gpu ' + str(typ), fontsize=16)
	# plt.grid(True)
	# plt.savefig('./rnn/gpu'+str(typ)+'.png', bbox_inches='tight', dpi=300)
	# plt.show()

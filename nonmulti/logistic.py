from __future__ import print_function
import tensorflow as tf
import os

from tensorflow.examples.tutorials.mnist import input_data

# define the command line flags that can be sent
tf.app.flags.DEFINE_integer("task_index", 0, "Index of task with in the job.")
tf.app.flags.DEFINE_string("job_name", "worker", "either worker or ps")
tf.app.flags.DEFINE_string("deploy_mode", "single", "either single or cluster")
FLAGS = tf.app.flags.FLAGS

tf.logging.set_verbosity(tf.logging.DEBUG)

clusterSpec_single = tf.train.ClusterSpec({
    "worker" : [
        "localhost:2222"
    ]
})

clusterSpec_cluster = tf.train.ClusterSpec({
    "ps" : [
        "10.10.1.1:2222"
    ],
    "worker" : [
        "10.10.1.1:2223",
        "10.10.1.2:2222"
    ]
})

clusterSpec_cluster2 = tf.train.ClusterSpec({
    "ps" : [
        "10.10.1.1:2222"
    ],
    "worker" : [
        "10.10.1.1:2223",
        "10.10.1.2:2222",
        "10.10.1.3:2222",
    ]
})

clusterSpec = {
    "single": clusterSpec_single,
    "cluster": clusterSpec_cluster,
    "cluster2": clusterSpec_cluster2
}

clusterinfo = clusterSpec[FLAGS.deploy_mode]
server = tf.train.Server(clusterinfo, job_name=FLAGS.job_name, task_index=FLAGS.task_index)

if FLAGS.job_name == "ps":
    server.join()
elif FLAGS.job_name == "worker":
    #put your code here
    def train():
        # define parameters
        learning_rate = 0.5
        training_epochs = 100000
        log_dir = '/tmp/tf/mnist/logs'

        # import data
        mnist = input_data.read_data_sets('/tmp/tf/mnist/input_data', one_hot=True)
        sess = tf.compat.v1.InteractiveSession()
        # placeholders
        with tf.compat.v1.name_scope('input'):
            x = tf.compat.v1.placeholder(tf.float32, [None,784], name='x_input')
            y_ = tf.compat.v1.placeholder(tf.float32, [None, 10], name='y-input') 

        # with tf.compat.v1.name_scope('input_reshape'):
        #     image_shaped_input = tf.reshape(x, [-1, 28, 28, 1])
        #     tf.compat.v1.summary.image('input', image_shaped_input, 10)

        def weight_variable(shape):
            initial = tf.random.truncated_normal(shape, stddev=0.1)
            return tf.Variable(initial)

        def bias_variable(shape):
            initial = tf.constant(0.0, shape=shape)
            return tf.Variable(initial)

        def variable_summaries(var):
            with tf.compat.v1.name_scope('summaries'):
                mean = tf.reduce_mean(input_tensor=var)
                tf.compat.v1.summary.scalar('mean', mean)
            with tf.compat.v1.name_scope('stddev'):
                stddev = tf.sqrt(tf.reduce_mean(input_tensor=tf.square(var - mean)))
                tf.compat.v1.summary.scalar('stddev', stddev)
                tf.compat.v1.summary.scalar('max', tf.reduce_max(input_tensor=var))
                tf.compat.v1.summary.scalar('min', tf.reduce_min(input_tensor=var))
                tf.compat.v1.summary.histogram('histogram', var)

        def logistic_regression(input_tensor, input_dim, output_dim):
            with tf.compat.v1.name_scope('weights'):
                weights = weight_variable([input_dim, output_dim])
                variable_summaries(weights)
            with tf.compat.v1.name_scope('bias'):
                bias = bias_variable([output_dim])
                variable_summaries(bias)
            with tf.compat.v1.name_scope('wx_plusb'):
                multiply = tf.matmul(input_tensor, weights) + bias
                tf.compat.v1.summary.histogram('multiply', multiply) 
            with tf.compat.v1.name_scope('prediction'):
                prediction = tf.nn.softmax(multiply)
                tf.compat.v1.summary.histogram('prediction', prediction)
            return prediction

        y = logistic_regression(x, 784, 10)
        with tf.compat.v1.name_scope('loss'):
            loss = tf.reduce_mean(-tf.reduce_sum(y_ * tf.math.log(y), reduction_indices=[1]))
            # loss = tf.compat.v1.losses.sparse_softmax_cross_entropy(labels=y_,logits=y)
        tf.compat.v1.summary.scalar('loss', loss)
        with tf.compat.v1.name_scope('train'):
            train_step = tf.compat.v1.train.GradientDescentOptimizer(learning_rate).minimize(loss)

        with tf.compat.v1.name_scope('accuracy'):
            with tf.compat.v1.name_scope('correct_prediction'):
                correct_prediction = tf.equal(tf.argmax(y, 1), tf.argmax(y_, 1))
            with tf.compat.v1.name_scope('accuracy'):
                accuracy = tf.reduce_mean(input_tensor=tf.cast(correct_prediction, tf.float32))
        tf.compat.v1.summary.scalar('accuracy', accuracy)
        # merge all summaries into one
        merged = tf.compat.v1.summary.merge_all()
        train_writer = tf.compat.v1.summary.FileWriter(log_dir + '/train', sess.graph)
        test_writer = tf.compat.v1.summary.FileWriter(log_dir + '/test')
        tf.compat.v1.global_variables_initializer().run()

        def feed_dict(train):
            if train:
                xs, ys = mnist.train.next_batch(100)
            else:
                xs, ys = mnist.test.images, mnist.test.labels
            return {x: xs, y_: ys}
        
        for i in range(training_epochs):
            if i % 10 == 0:
                summary, los = sess.run([merged, loss], feed_dict=feed_dict(False))
                test_writer.add_summary(summary, i)
                print('Loss at step %s: %s' %  (i, los))
            else:
                if i % 100 == 99:
                    run_options = tf.compat.v1.RunOptions(trace_level=tf.compat.v1.RunOptions.FULL_TRACE)
                    run_metadata = tf.compat.v1.RunMetadata()
                    summary, _ = sess.run([merged, train_step], feed_dict=feed_dict(True), options=run_options, run_metadata=run_metadata)
                    train_writer.add_run_metadata(run_metadata, 'step%03d' % i)
                    train_writer.add_summary(summary, i)
                    print('Metadata added at ', i)
                else:
                    summary, _ = sess.run([merged, train_step], feed_dict=feed_dict(True))
                    train_writer.add_summary(summary, i)
        train_writer.close()
        test_writer.close()
    def main(_):
        log_dir = '/tmp/tf/mnist/logs'
        if tf.io.gfile.exists(log_dir):
            tf.io.gfile.rmtree(log_dir)
        tf.io.gfile.makedirs(log_dir)
        with tf.Graph().as_default():
            train()

    if __name__ == '__main__':
        tf.compat.v1.app.run(main=main)

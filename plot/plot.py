import matplotlib.pyplot as plt
import numpy as np
import os
import sys
from os.path import expanduser

def get_cpu_mem_net(input):
    with open(input, 'r') as f:
        lines = [line for line in f]
        lines = lines[8:]
        content = np.loadtxt(lines, delimiter=',')
        m, n = content.shape
    return m, content[:,0], content[:,6], content[:,10], content[:,11]

if __name__ == '__main__':
    res    = sys.argv[1]
    num_cluster  = sys.argv[2]
    m      = sys.argv[3]
    fig    = plt.figure()
    length = 0
    maps   = {'cpu':'CPU', 'mem':'Memory', 'recv':'Recv', 'send':'Send'}
    unit   = {'cpu':1, 'mem':1/3e10, 'recv':8/1e7, 'send':8/1e7}
    for key in maps.keys():
        plt.clf()
        for i in range(int(num_cluster)):
            length, cpu, mem, recv, send = get_cpu_mem_net(res+'node'+str(i)+'.csv')
            length = min(int(m),length)
            time = np.arange(length)
            # print(np.linalg.norm(recv-send)/len(send))
            plt.plot(time, eval(key)[:length]*unit[key],label='node'+str(i),linewidth=1.5)

        plt.legend(prop={'size':12})
        plt.xlabel('Time (Sec)',fontsize=14)
        plt.ylabel(maps[key]+'(Percent)',fontsize=14)
        plt.grid(True)
        # plt.show()
        plt.savefig("lenet/{!s}/".format(num_cluster) + key + ".png", bbox_inches='tight', dpi=300)

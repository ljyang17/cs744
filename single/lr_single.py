from __future__ import print_function
import tensorflow as tf
from tensorflow.examples.tutorials.mnist import input_data
from tensorflow.python.training.summary_io import SummaryWriterCache

# define the command line flags that can be sent
tf.app.flags.DEFINE_integer("task_index", 0, "Index of task with in the job.")
tf.app.flags.DEFINE_string("job_name", "worker", "either worker or ps")
tf.app.flags.DEFINE_string("deploy_mode", "single", "either single or cluster")
FLAGS = tf.app.flags.FLAGS

tf.logging.set_verbosity(tf.logging.DEBUG)

clusterSpec_single = tf.train.ClusterSpec({
    "worker": [
        "localhost:6666"
    ]
})

clusterSpec_cluster = tf.train.ClusterSpec({
    "ps": [
        "node0:2222"
    ],
    "worker": [
        "node0:2223",
        "node1:2222"
    ]
})

clusterSpec_cluster2 = tf.train.ClusterSpec({
    "ps": [
        "node0:2222"
    ],
    "worker": [
        "node0:2223",
        "node1:2222",
        "node2:2222",
    ]
})

clusterSpec = {
    "single": clusterSpec_single,
    "cluster": clusterSpec_cluster,
    "cluster2": clusterSpec_cluster2
}

workerNum = {
    "single": 1,
    "cluster": 2,
    "cluster2": 3
}

clusterinfo = clusterSpec[FLAGS.deploy_mode]
server = tf.train.Server(clusterinfo, job_name=FLAGS.job_name, task_index=FLAGS.task_index)

if FLAGS.job_name == "ps":
    server.join()
elif FLAGS.job_name == "worker":
    def train():
        # define parameters
        learning_rate = 0.025
        training_epochs = 20
        batch_size = 50
        log_dir = '/tmp/tf/mnist/logs/single'

        # get input data
        mnist = input_data.read_data_sets("/tmp/tf/mnist/input_data", one_hot=True)

        def weight_variable(shape):
            initial = tf.random.truncated_normal(shape, stddev=0.1)
            return tf.Variable(initial)
        def bias_variable(shape):
            initial = tf.constant(0.0, shape=shape)
            return tf.Variable(initial)
        # summaries for tensorboard visualization
        def variable_summaries(var):
            with tf.compat.v1.name_scope('summaries'):
                mean = tf.reduce_mean(input_tensor=var)
                tf.compat.v1.summary.scalar('mean', mean)
            with tf.compat.v1.name_scope('stddev'):
                stddev = tf.sqrt(tf.reduce_mean(input_tensor=tf.square(var - mean)))
                tf.compat.v1.summary.scalar('stddev', stddev)
                tf.compat.v1.summary.scalar('max', tf.reduce_max(input_tensor=var))
                tf.compat.v1.summary.scalar('min', tf.reduce_min(input_tensor=var))
                tf.compat.v1.summary.histogram('histogram', var)
        # lr core operations
        def logistic_regression(input_tensor, input_dim, output_dim):
            with tf.compat.v1.name_scope('weights'):
                weights = weight_variable([input_dim, output_dim])
                variable_summaries(weights)
            with tf.compat.v1.name_scope('bias'):
                bias = bias_variable([output_dim])
                variable_summaries(bias)
            with tf.compat.v1.name_scope('wx_plusb'):
                multiply = tf.matmul(input_tensor, weights) + bias
                tf.compat.v1.summary.histogram('multiply', multiply)
            with tf.compat.v1.name_scope('prediction'):
                prediction = tf.nn.softmax(multiply)
                tf.compat.v1.summary.histogram('prediction', prediction)
            return prediction
        # if train or test, feed different data 
        def feed_dict(train):
            if train:
                xs, ys = mnist.train.next_batch(50)
            else:
                xs, ys = mnist.test.images, mnist.test.labels
            return {x: xs, y_: ys}
        # set device
        with tf.device(tf.train.replica_device_setter(
                worker_device="/job:worker/task:%d" % FLAGS.task_index,
                cluster=clusterinfo)):
            with tf.compat.v1.name_scope('input'):
                # 28*28=784, input tensor size
                x = tf.compat.v1.placeholder(tf.float32, [None,784], name='x_input')
                # 0-9 has 10 digits, placeholder for labels
                y_ = tf.compat.v1.placeholder(tf.float32, [None, 10], name='y-input')

            y = logistic_regression(x, 784, 10)
            with tf.compat.v1.name_scope('loss'):
                loss = tf.reduce_mean(-tf.reduce_sum(y_ * tf.math.log(y), reduction_indices=[1]))
            tf.compat.v1.summary.scalar("loss", loss)

            global_step = tf.train.get_or_create_global_step()
            # compute accuracy and add error to summary
            correct_predction = tf.equal(tf.argmax(y, 1), tf.argmax(y_, 1))
            accuracy = tf.reduce_mean(tf.cast(correct_predction, tf.float32))            
            tf.compat.v1.summary.scalar("error",1-accuracy)
            merged = tf.compat.v1.summary.merge_all()
            # determine number of steps to stop
            numExamples = mnist.train.num_examples
            numBatch = numExamples // batch_size
            hooks = [tf.train.StopAtStepHook(last_step=numBatch*training_epochs)]
            # define optimizer
            optimizer = tf.train.GradientDescentOptimizer(learning_rate)
            # optimizer = tf.train.SyncReplicasOptimizer(optimizer, replicas_to_aggregate=workerNum[FLAGS.deploy_mode],
            #                                                   total_num_replicas=workerNum[FLAGS.deploy_mode])
            mini = optimizer.minimize(loss, global_step=global_step)
            # hooks.append(optimizer.make_session_run_hook((FLAGS.task_index==0)))
            # initialize scaffold
            scaffold = tf.train.Scaffold(init_op=tf.global_variables_initializer(),
                                         local_init_op=tf.local_variables_initializer())

            with tf.train.MonitoredTrainingSession(master=server.target,
                                                   is_chief=(FLAGS.task_index == 0),
                                                   checkpoint_dir=log_dir,
                                                   hooks=hooks, scaffold=scaffold) as monSess:
                step = 0
                # add session graph to summary
                SummaryWriterCache.get(log_dir).add_graph(monSess.graph)
                while not monSess.should_stop():
                    # training operations and summary
                    summary_train, _, l_train, step = monSess.run([merged, mini, loss, global_step], feed_dict=feed_dict(True))
                    SummaryWriterCache.get(log_dir + "/train").add_summary(summary_train, step)
                    if step % 6 == 0:
                        # test operations and summary
                        summary_test, _, l_test, step = monSess.run([merged, mini, loss, global_step], feed_dict=feed_dict(False))
                        SummaryWriterCache.get(log_dir + "/test").add_summary(summary_test, step)
                    print("Loss: ", l_train, "step: ", step)
                    step += 1
    def main(_):
        # initialize log directory
        log_dir = '/tmp/tf/mnist/logs/single'
        if tf.io.gfile.exists(log_dir):
            tf.io.gfile.rmtree(log_dir)
        tf.io.gfile.makedirs(log_dir)
        with tf.Graph().as_default():
            train()
    if __name__ == '__main__':
        tf.compat.v1.app.run(main=main)

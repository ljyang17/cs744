#!/bin/bash

export TF_LOG_DIR="/tmp/tf/mnist/logs/async"
# mkdir $TF_LOG_DIR
# cluster_utils.sh has helper function to start process on all VMs
# it contains definition for start_cluster and terminate_cluster
source cluster_utils.sh
start_cluster lr_async.py cluster2

tensorboard --logdir $TF_LOG_DIR
# defined in cluster_utils.sh to terminate the cluster
terminate_cluster
terminate_perf

#!/bin/bash

export TF_LOG_DIR="/tmp/lenet/mnist/logs"
# cluster_utils.sh has helper function to start process on all VMs
# it contains definition for start_cluster and terminate_cluster
source cluster_utils.sh
start_cluster lenet.py $1 

terminate_perf
tensorboard --logdir $TF_LOG_DIR
# defined in cluster_utils.sh to terminate the cluster
terminate_cluster

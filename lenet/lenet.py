import json
import os
import numpy as np
import tensorflow as tf
from tensorflow import keras
from tensorflow.keras import layers


# define the command line flags that can be sent
tf.app.flags.DEFINE_integer("task_index", 0, "Index of task with in the job.")
tf.app.flags.DEFINE_string("job_name", "worker", "either worker or ps")
tf.app.flags.DEFINE_string("deploy_mode", "single", "either single or cluster")
FLAGS = tf.app.flags.FLAGS

tf.logging.set_verbosity(tf.logging.DEBUG)
log_dir = "/tmp/lenet/mnist/logs"
if tf.io.gfile.exists(log_dir):
    tf.io.gfile.rmtree(log_dir)
tensorboard_callback = keras.callbacks.TensorBoard(log_dir=log_dir)

batch_size = 1024*32 
evaluate_steps = int(np.floor(10000 / float(batch_size) / 3))
train_steps = int(np.ceil(60000 / float(batch_size) / 3))

worker_list = ["10.10.1.1:4222", "10.10.1.2:4222", "10.10.1.3:4222"]
if FLAGS.deploy_mode == "single":
    worker_list = worker_list[0:1]
elif FLAGS.deploy_mode == "cluster":
    worker_list = worker_list[0:2]
    
os.environ["TF_CONFIG"] = json.dumps({
    "cluster": {
        "worker": worker_list,
    },
   "task": {"type": "worker", "index": FLAGS.task_index}
})


#put your code here
mirrored_strategy = tf.distribute.experimental.MultiWorkerMirroredStrategy()

with mirrored_strategy.scope():
    (x_train, y_train), (x_test, y_test) = keras.datasets.mnist.load_data()
    # Preprocess the data (these are Numpy arrays)
    x_train = x_train.reshape(60000, 28, 28, 1).astype('float32') / 255
    x_test = x_test.reshape(10000, 28, 28, 1).astype('float32') / 255
    
    y_train = keras.utils.to_categorical(y_train, 10)
    y_test = keras.utils.to_categorical(y_test, 10)
    
    # Reserve 10,000 samples for validation
    x_val = x_train[-10000:]
    y_val = y_train[-10000:]
    x_train = x_train[:-10000]
    y_train = y_train[:-10000]
    train_dataset = tf.data.Dataset.from_tensor_slices((x_train, y_train))
    train_dataset = train_dataset.repeat()
    train_dataset = train_dataset.batch(batch_size*3)

    test_dataset = tf.data.Dataset.from_tensor_slices((x_test, y_test))
    test_dataset = test_dataset.repeat()
    test_dataset = test_dataset.batch(max(batch_size*3, 10000))
    
    model = tf.keras.Sequential([
	    layers.Conv2D(filters=6, kernel_size=(3, 3), activation='relu', input_shape=(28, 28,1)),
	    layers.AveragePooling2D(),
	    layers.Conv2D(filters=16, kernel_size=(3, 3), activation='relu'),
	    layers.AveragePooling2D(),
	    layers.Flatten(),
	    layers.Dense(units=120, activation='relu'),
	    layers.Dense(units=84, activation='relu'),
	    layers.Dense(units=10, activation='softmax')
	])
    model.compile(optimizer=tf.keras.optimizers.Adam(0.01),
        loss=tf.keras.losses.CategoricalCrossentropy(),
        metrics=[tf.keras.metrics.CategoricalAccuracy()])
   
    print("Training")
    history = model.fit(train_dataset, epochs=10, steps_per_epoch=train_steps, verbose=2, callbacks=[tensorboard_callback])
    print('History dict:', history.history)
    print("Evaluating")
    result = model.evaluate(test_dataset, steps=evaluate_steps, callbacks=[tensorboard_callback])
    print("Result:", result)
         

